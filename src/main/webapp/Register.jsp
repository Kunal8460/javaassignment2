<%-- 
    Document   : Register
    Created on : 13-Feb-2023, 2:33:00 PM
    Author     : root
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="jc" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet"  integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    </head>
    <body>
        <section class="h-auto" style="background-color: #8fc4b7;">
            <div class="container h-auto py-5">
                <div class=" row d-flex justify-content-center align-items-center h-auto">
                    <div class="col-lg-8 col-xl-6 ">
                        <div class="card overflow-auto rounded-3" style="height:90vh;">
                            <div class="card-body p-4 p-md-5">
                                <h2 class="fw-semi-bold mb-4 pb-2 pb-md-0 mb-md-5 px-md-2">Registration</h2>
                                <form method='post' class="px-md-2">
                                    <div class="row">
                                        <label class="form-label" for="form3Example1q">Username</label>
                                        <div class="form-outline mb-4">
                                            <input type="text" id="username" name="username" class="form-control" />
                                        </div>
                                        <div class="col-md-12">
                                            <label class="form-label" for="password">Password</label>
                                            <div class="form-outline mb-4">
                                                <input type="password" id="password" name="password" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <label class="form-label" for="password_question">Password</label>
                                            <select class="form-select" name="password_question">
                                                <option value="" selected>Select Question</option>
                                                <option value="What is you favourite food?">What is you favourite food?
                                                </option>
                                                <option value="What is your favourite animal?">What is your favourite
                                                    animal?</option>
                                                <option value="What is your favourite movie?">What is your favourite movie?
                                                </option>
                                            </select>
                                        </div>
                                        <div class="col-md-12 mt-4">
                                            <label class=\"form-label\" for="password_answer">Security Answer</label>
                                            <input type="text" class="form-control" name="password_answer" />
                                        </div>
                                        <div class="col-md-12 mt-4">
                                            <label class="form-label" for="email">Email</label>
                                            <input type="email" class="form-control" name="email" />
                                        </div>
                                        <div class="col-md-12 mt-4">
                                            <label class="form-label" for="phone">Phone</label>
                                            <input type="text" class="form-control" name="phone" />
                                        </div>
                                        <div class="col-md-12 mt-4">
                                            <label class="form-label" for="address">Address</label>
                                            <input type="text" class="form-control" name="address" />
                                        </div>
                                        <div class="col-md-12 mt-4">
                                            <label class="form-label" for="country">Country</label>
                                            <select class="form-select" name="country" id="country">
                                                <option value="" selected>Select Country</option>

                                            </select>
                                        </div>
                                        <div class="col-md-12 mt-4">
                                            <label class="form-label" for="state">State</label>
                                            <select class="form-select" name="state">
                                                <option value="" selected>Select State</option>
                                            </select>
                                        </div>
                                        <div class="col-md-12 mt-4">
                                            <label class="form-label" for="city">City</label>
                                            <select class="form-select" name="city">
                                                <option value="" selected>Select City</option>
                                            </select>
                                        </div>
                                        <div class="col-md-12 mt-4">
                                            <button type="submit" class="mt-4 btn btn-success btn-md mb-1">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <script lang="javascript">

        </script>
    </body>
</html>
