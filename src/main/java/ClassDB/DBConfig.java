/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ClassDB;

import java.sql.*;
import java.util.*;

/**
 *
 * @author root
 */
public class DBConfig {

    private static Connection con;
//    PreparedStatement ptst = null;
//    String sql;

    public DBConfig() {

    }

    public static Connection getCon() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/sem8_12", "root", "root");
            if (!con.isClosed()) {
                System.out.println("Connection successfull");
                return con;
            }
        } catch (Exception ex) {
            System.out.println(ex);
        }
        return con;
    }

//    public int Insert(String tableName, LinkedList<String> records) {
//        sql = "Insert into " + tableName + " values ";
//       
//        int result = 0;
//        try {
//
//            System.out.println(sql);
//            ptst = con.prepareStatement(sql);
//            result = ptst.executeUpdate();
//
//        } catch (Exception ex) {
//            System.out.println(ex);
//        }
//        return result;
//    }
}
