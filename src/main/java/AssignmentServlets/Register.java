/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package AssignmentServlets;

//import ClassDB.DBConfig;
import ClassDB.DBConfig;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.sql.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author root
 */
@WebServlet(name = "Register", urlPatterns = {"/Register"})
public class Register extends HttpServlet {

    Connection connection = null;
    PreparedStatement ptst = null;

    public Register() {
        DBConfig db = new DBConfig();
        connection = db.getCon();
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        System.out.println(success);
//    }
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Register</title>");
            out.println("</head>");
            out.println("<link href=\"https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css\" rel=\"stylesheet\"\n"
                    + "        integrity=\"sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD\" crossorigin=\"anonymous\">");
            out.println("<body>");
            out.println("<section class=\"h-auto\" style=\"background-color: #8fc4b7;\">\n"
                    + "        <div class=\"container h-auto py-5\">\n"
                    + "            <div class=\" row d-flex justify-content-center align-items-center h-auto\">\n"
                    + "                <div class=\"col-lg-8 col-xl-6 \">\n"
                    + "                    <div class=\"card overflow-auto rounded-3\" style=\"height:90vh;\">\n"
                    + "                        <!-- <img src=\"https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-registration/img3.webp\"\n"
                    + "                        class=\"w-100\" style=\"border-top-left-radius: .3rem; border-top-right-radius: .3rem;\"\n"
                    + "                        alt=\"Sample photo\"> -->\n"
                    + "                        <div class=\"card-body p-4 p-md-5\">\n"
                    + "                            <h2 class=\"fw-semi-bold mb-4 pb-2 pb-md-0 mb-md-5 px-md-2\">Registration</h2>\n"
                    + "                            <form method='post' class=\"px-md-2\">\n"
                    + "                                <div class=\"row\">\n"
                    + "                                    <label class=\"form-label\" for=\"form3Example1q\">Username</label>\n"
                    + "                                    <div class=\"form-outline mb-4\">\n"
                    + "                                        <input type=\"text\" id=\"username\" name=\"username\" class=\"form-control\" />\n"
                    + "                                    </div>\n"
                    + "                                    <div class=\"col-md-12\">\n"
                    + "                                        <label class=\"form-label\" for=\"password\">Password</label>\n"
                    + "                                        <div class=\"form-outline mb-4\">\n"
                    + "                                            <input type=\"password\" id=\"password\" name=\"password\" class=\"form-control\" />\n"
                    + "                                        </div>\n"
                    + "                                    </div>\n"
                    + "                                    <div class=\"col-md-12\">\n"
                    + "                                        <label class=\"form-label\" for=\"password_question\">Password</label>\n"
                    + "                                        <select class=\"form-select\" name=\"password_question\">\n"
                    + "                                            <option value=\"\" selected>Select Question</option>\n"
                    + "                                            <option value=\"What is you favourite food?\">What is you favourite food?\n"
                    + "                                            </option>\n"
                    + "                                            <option value=\"What is your favourite animal?\">What is your favourite\n"
                    + "                                                animal?</option>\n"
                    + "                                            <option value=\"What is your favourite movie?\">What is your favourite movie?\n"
                    + "                                            </option>\n"
                    + "                                        </select>\n"
                    + "                                    </div>\n"
                    + "                                    <div class=\"col-md-12 mt-4\">\n"
                    + "                                        <label class=\"form-label\" for=\"password_answer\">Security Answer</label>\n"
                    + "                                        <input type=\"text\" class=\"form-control\" name=\"password_answer\" />\n"
                    + "                                    </div>\n"
                    + "                                    <div class=\"col-md-12 mt-4\">\n"
                    + "                                        <label class=\"form-label\" for=\"email\">Email</label>\n"
                    + "                                        <input type=\"email\" class=\"form-control\" name=\"email\" />\n"
                    + "                                    </div>\n"
                    + "                                    <div class=\"col-md-12 mt-4\">\n"
                    + "                                        <label class=\"form-label\" for=\"phone\">Phone</label>\n"
                    + "                                        <input type=\"text\" class=\"form-control\" name=\"phone\" />\n"
                    + "                                    </div>\n"
                    + "                                    <div class=\"col-md-12 mt-4\">\n"
                    + "                                        <label class=\"form-label\" for=\"address\">Address</label>\n"
                    + "                                        <input type=\"text\" class=\"form-control\" name=\"address\" />\n"
                    + "                                    </div>\n"
                    + "                                    <div class=\"col-md-12 mt-4\">\n"
                    + "                                        <label class=\"form-label\" for=\"country\">Country</label>\n"
                    + "                                        <select class=\"form-select\" name=\"country\">\n"
                    + "                                            <option value=\"\" selected>Select Country</option>\n");
                                                            try {
//                                                            ArrayList<String> countries = new ArrayList<>();
                                                               
                                                                ptst = connection.prepareStatement("select * from country_master");
                                                                 ResultSet rs =  ptst.executeQuery();
                                                                
                                                                while(rs.next()){
                                                                    String country = rs.getString("name");
                                                                    out.println("<option value='"+ country +"' >"+ country +"</option>\n");
                                                                }

                                                            } catch (Exception ex) {
                                                                System.out.println(ex);

                                                            }
                    out.println("                                        </select>\n"
                    + "                                    </div>\n"
                    + "                                    <div class=\"col-md-12 mt-4\">\n"
                    + "                                        <label class=\"form-label\" for=\"state\">State</label>\n"
                    + "                                        <select class=\"form-select\" name=\"state\">\n"
                    + "                                            <option value=\"\" selected>Select State</option>\n"
                    + "                                        </select>\n"
                    + "                                    </div>\n"
                    + "                                    <div class=\"col-md-12 mt-4\">\n"
                    + "                                        <label class=\"form-label\" for=\"city\">City</label>\n"
                    + "                                        <select class=\"form-select\" name=\"city\">\n"
                    + "                                            <option value=\"\" selected>Select City</option>\n"
                    + "                                        </select>\n"
                    + "                                    </div>\n"
                    + "                                    <div class=\"col-md-12 mt-4\">\n"
                    + "                                        <button type=\"submit\" class=\"mt-4 btn btn-success btn-md mb-1\">Submit</button>\n"
                    + "                                    </div>\n"
                    + "                                </div>\n"
                    + "                            </form>\n"
                    + "                        </div>\n"
                    + "                    </div>\n"
                    + "                </div>\n"
                    + "            </div>\n"
                    + "        </div>\n"
                    + "    </section>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

//        LinkedList<String> record = new LinkedList<>();
//        record.add(request.getParameter("username"));
//        record.add(request.getParameter("password"));
//        record.add(request.getParameter("password_question"));
//        record.add(request.getParameter("password_answer"));
//        record.add(request.getParameter("email"));
//        record.add(request.getParameter("phone"));
//        record.add(request.getParameter("address"));
//        DBConfig dbcon = new DBConfig();
//        int success = dbcon.Insert("user_master", record);
//        System.out.println(success);
        String sql = "insert into user_master (user_name,password,password_question,password_answer,email,phone,addresss,country) values "
                + "('" + request.getParameter("username")
                + "','" + request.getParameter("password")
                + "','" + request.getParameter("password_question")
                + "','" + request.getParameter("password_answer")
                + "','" + request.getParameter("email")
                + "','" + request.getParameter("phone")
                + "','" + request.getParameter("address")
                + "','" + request.getParameter("country")
                + "')";

        try {

            ptst = connection.prepareStatement(sql);
            ptst.executeUpdate();

        } catch (Exception ex) {
            System.out.println(ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public ArrayList getcountries() {
        ArrayList<String> countries = new ArrayList<>();
        try {

            ptst = connection.prepareStatement("select * from country_master");
            countries = (ArrayList<String>) ptst.executeQuery();

        } catch (Exception ex) {
            System.out.println(ex);

        }
        return countries;
    }
}
