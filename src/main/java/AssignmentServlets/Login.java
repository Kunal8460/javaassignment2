/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package AssignmentServlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author root
 */
@WebServlet(name = "Login", urlPatterns = {"/Login"})
public class Login extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!doctype html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<meta charset = 'utf-8'>");
            out.println("<meta name = 'viewport' content = 'width=device-width, initial-scale=1'>");
            out.println("<title>Login</title>");
            out.println("<link href = 'https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css' rel = 'stylesheet' integrity = 'sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD' crossorigin = 'nonymous'>");
            out.println("</head>");
            out.println("<body class='my-5'>");
            out.println("<div class=\"container bg-secondary bg-opacity-25 py-5 border border-success-subtle rounded-3 shadow-lg\"  >");
            out.println("<h3>Login</h3>");
            out.println("<div class=\"container p-4\">");
            out.println("<form action=\"\">");
            out.println("<div class=\"mb-3 row\">");
            out.println("<label for=\"email\" class=\"col-sm-2 col-form-label\">Email</label>");
            out.println("<div class=\"col-sm-6\">");
            out.println("<input type=\"text\" class=\"form-control\" name=\"email\" id=\"email\" placeholder=\"abc@gmail.com\">");
            out.println("</div>");
            out.println("</div>");
            out.println("<div class=\"mb-3 row\">");
            out.println("<label for=\"password\" class=\"col-sm-2 col-form-label\">Password</label>");
            out.println("<div class=\"col-sm-6\">");
            out.println("<input type=\"password\" name=\"password\" class=\"form-control\" id=\"password\">");
            out.println("</div>");
            out.println("</div>");
            out.println("<div class=\"mb-3 row\">");
            out.println("<div class=\"col-sm-12\">");
            out.println("<input type=\"submit\" value=\"Login\" class=\"btn btn-success\">");
            out.println("<a href='Register' class='btn btn-primary' > Register </a>");
            out.println("</div>");
            out.println("</div>");
            out.println("</form>");
            out.println("</div>");
            out.println("</div>");
            out.println("</div>");   
            out.println("</div");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
