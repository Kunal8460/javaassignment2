/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package city_state_country;

import java.sql.*;
import java.util.*;

/**
 *
 * @author root
 */
public class Location_API {

    Connection con;
    PreparedStatement ptst;
    ResultSet rs;
    String sql;

    public Location_API(Connection con) {
        this.con = con;
    }

    public List<Country> getAllCountries() {
        List<Country> countries = new ArrayList<Country>();
        try {

            sql = "Select * from country_master";
            ptst = con.prepareStatement(sql);
            rs = ptst.executeQuery();
            while (rs.next()) {
                Country country = new Country();
                country.setId(rs.getInt("id"));
                country.setName(rs.getString("name"));
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return countries;
    }

    public List<State> getStateByCountry(int countryId) {
        List<State> states = new ArrayList<State>();
        try {

            sql = "Select * from state_master where country_id = ?";
            ptst = con.prepareStatement(sql);
            ptst.setInt(1,countryId);
            rs = ptst.executeQuery();
            while (rs.next()) {
                State state = new State();
                state.setId(rs.getInt("id"));
                state.setName(rs.getString("name"));
                state.setCountry_id(rs.getInt("country_id"));
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return states;
    }
    
    public List<City> getCityByState(int stateId) {
        List<City> cities = new ArrayList<City>();
        try {

            sql = "Select * from city_master where state_id = ?";
            ptst = con.prepareStatement(sql);
            ptst.setInt(1,stateId);
            rs = ptst.executeQuery();
            while (rs.next()) {
                City city = new City();
                city.setId(rs.getInt("id"));
                city.setName(rs.getString("name"));
                city.setState_id(rs.getInt("state_id"));
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return cities;
    }
}
