/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package city_state_country;

/**
 *
 * @author root
 */
public class City {

    int id,state_id;
    String name;

    public City() {
    }

    public City(int id, int state_id, String name) {
        this.id = id;
//        this.country_id = country_id;
        this.state_id = state_id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

//    public int getCountry_id() {
//        return country_id;
//    }
//
//    public void setCountry_id(int country_id) {
//        this.country_id = country_id;
//    }

    public int getState_id() {
        return state_id;
    }

    public void setState_id(int state_id) {
        this.state_id = state_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
